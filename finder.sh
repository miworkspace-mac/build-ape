#!/bin/bash


NEWLOC=`curl -L https://jorgensen.biology.utah.edu/wayned/ape/Mac_opening_instructions.html  2>/dev/null | /usr/local/bin/htmlq -a href a | grep dmg | head -1`


if [ "x${NEWLOC}" != "x" ]; then
	echo https://jorgensen.biology.utah.edu"${NEWLOC}"
fi